# README #

This repository contains base RAML documents that can be shared by any Impulse projects.  Types defined here a not project specific, they are either general types that are needed in multiple places, or are base types that are meant to be extended for use in individual APIs.